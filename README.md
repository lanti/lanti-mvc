# MVC

```sh
$ go get -u github.com/gorilla/mux
$ go get -u github.com/gorilla/handlers
$ go get -u github.com/gorilla/sessions
$ go get -u github.com/flosch/pongo2
$ go get -u github.com/flosch/pongo2-addons
```

Documentation:

https://github.com/gorilla/mux#examples

https://github.com/gorilla/handlers

Not Found handler ideas:

https://github.com/gorilla/handlers/issues/83
