package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	controllers "gitlab.com/Lanti/lanti-mvc/src/controllers"
	mw "gitlab.com/Lanti/lanti-mvc/src/middlewares"
)

func main() {
	// json parsing
	file, err := ioutil.ReadFile("./config.json")
	mw.Check(err)
	var f interface{}
	json.Unmarshal([]byte(file), &f)
	config := f.(map[string]interface{})
	//print := fmt.Sprintf("%s:%g", config["host"], config["port"])
	//fmt.Println(print)

	// https://github.com/gorilla/mux#static-files
	var dir string
	flag.StringVar(&dir, "dir", "./static", "the directory to serve files from. Defaults to ./static dir")
	flag.Parse()

	renderer := mw.Renderer{
		Debug: false,
	}

	r := mux.NewRouter()
	//r.NotFoundHandler = http.HandlerFunc(controllers.NotFoundHandler)
	r.NotFoundHandler = http.HandlerFunc(controllers.NotFoundHandler)

	s := r.Methods("GET").Subrouter()
	//s.HandleFunc("/", controllers.Index)
	s.HandleFunc("/", controllers.Index(&renderer))

	a := r.PathPrefix("/articles").Subrouter()
	a.HandleFunc("/", controllers.Articles).Methods("GET")
	a.HandleFunc("/{category}/", controllers.ArticlesCategory).Methods("GET")

	// https: //github.com/gorilla/mux#static-files
	// This will serve files under http://localhost:8000/static/<filename>
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))

	loggedRouter := handlers.CombinedLoggingHandler(os.Stdout, r)
	srv := &http.Server{
		//Handler: r,
		Handler: handlers.CompressHandler(loggedRouter), // gzip all responses
		//Addr:    "127.0.0.1:8000",
		Addr: fmt.Sprintf("%s:%g", config["host"], config["port"]),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
