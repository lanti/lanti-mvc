package middlewares

/*
import (
	mw "github.com/djviolin/lanti-mvc/src/middlewares"
)

mw.Check(err)
*/

// Check : This helper will streamline our error checks below
func Check(e error) {
	if e != nil {
		//log.Fatal("ERROR: ", e)
		panic(e)
	}
}
