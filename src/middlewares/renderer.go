package middlewares

import (
	"io"

	"github.com/flosch/pongo2"
)

// https://github.com/flosch/pongo2
// https://machiel.me/post/pongo2-with-echo-or-net-http/

/*
import (
	mw "github.com/djviolin/lanti-mvc/src/middlewares"
)

renderer := mw.Renderer{
	Debug: false,
}

http.HandleFunc("/", Index(&renderer))
*/

// Renderer : struct
type Renderer struct {
	Debug bool
}

// Render : function
func (r Renderer) Render(w io.Writer, name string, data pongo2.Context) error {
	var t *pongo2.Template
	var err error

	if r.Debug {
		t, err = pongo2.FromFile(name)
	} else {
		t, err = pongo2.FromCache(name)
	}

	// Add some static values
	data["version_number"] = "v1.0.0"

	if err != nil {
		return err
	}

	return t.ExecuteWriter(data, w)
}
