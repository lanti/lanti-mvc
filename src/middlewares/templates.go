package middlewares

// https://github.com/siongui/userpages/blob/master/content/articles/2017/02/13/go-template-parse-all-files-in-directory%25en.rst

/*
import (
	mw "github.com/djviolin/lanti-mvc/src/middlewares"
)

mw.ParseDirectory("./views", "index")
*/

import (
	"html/template"
	"os"
	"path/filepath"
	"strings"
)

// GetAllFilePathsInDirectory : recursively get all file paths in dir + sub-dirs
func GetAllFilePathsInDirectory(dirpath string) ([]string, error) {
	var paths []string
	err := filepath.Walk(dirpath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.HasSuffix(path, ".html") && !info.IsDir() {
			paths = append(paths, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return paths, nil
}

// ParseDirectory : recursively parse all files in dir + sub-dirs
func ParseDirectory(dirpath string, file string) (*template.Template, error) {
	paths, err := GetAllFilePathsInDirectory(dirpath)
	if err != nil {
		return nil, err
	}
	//fmt.Println(paths) // logging
	t := template.New(file)
	return t.ParseFiles(paths...)
}

// ParseTemplates : https://stackoverflow.com/questions/38686583/golang-parse-all-templates-in-directory-and-subdirectories
// Usage: template.ExecuteTemplate(w, "home", nil)
/*func ParseTemplates() *template.Template {
	templ := template.New("")
	err := filepath.Walk("./views", func(path string, info os.FileInfo, err error) error {
		if strings.Contains(path, ".html") {
			_, err = templ.ParseFiles(path)
			if err != nil {
				log.Println(err)
			}
		}

		return err
	})

	if err != nil {
		panic(err)
	}

	return templ
}*/
