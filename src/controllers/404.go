package controllers

import (
	"log"
	"net/http"

	"github.com/flosch/pongo2"
)

// https://golang.org/src/net/http/server.go?s=58121:58151#L1953

// NotFound : http://127.0.0.1:8000/foo
/*func NotFound(renderer *mw.Renderer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data := pongo2.Context{
			"name": "Lanti",
			"answer": func() int {
				return 42
			},
			"items": []string{
				"Apple", "Pear", "Banana", "Pineapple",
			},
		}

		if err := renderer.Render(w, "views/index.html", data); err != nil {
			log.Printf("could not execute: %s", err)
		}
	}
}*/

// NotFoundHandler : Custom 404 error page
/*func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	// A Content-Type header has to be set before calling w.WriteHeader,
	// otherwise WriteHeader is called twice (from this handler and
	// the compression handler) and the response breaks.
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusNotFound)
	//fmt.Fprintf(w, "404 - not found")
	Error(w, "404 page not found", StatusNotFound)
}*/

// Error replies to the request with the specified error message and HTTP code.
// It does not otherwise end the request; the caller should ensure no further
// writes are done to w.
// The error message should be plain text.
/*func Error(w http.ResponseWriter, error string, code int) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	fmt.Fprintln(w, error)
}

// NotFoundHandler : http://127.0.0.1:8000/foo
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	// A Content-Type header has to be set before calling w.WriteHeader,
	// otherwise WriteHeader is called twice (from this handler and
	// the compression handler) and the response breaks.
	//w.Header().Set("Content-Type", "text/html; charset=utf-8")
	//w.WriteHeader(http.StatusNotFound)
	//fmt.Fprintf(w, "404 - not found")

	Error(w, "404 page not found", http.StatusNotFound)
}*/

// NotFoundHandler : http://127.0.0.1:8000/foo
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusNotFound)

	t, err := pongo2.FromFile("views/404.html")

	if err != nil {
		log.Printf("could not render: %s", err)
		return
	}

	data := pongo2.Context{
		"statusCode": http.StatusNotFound,
		"statusText": http.StatusText(http.StatusNotFound),
	}

	if err := t.ExecuteWriter(data, w); err != nil {
		log.Printf("could not execute: %s", err)
	}
}
