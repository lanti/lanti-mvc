package controllers

import (
	"log"
	"net/http"

	"github.com/flosch/pongo2"

	mw "gitlab.com/Lanti/lanti-mvc/src/middlewares"
)

// Student : constructor for template
/*type Student struct {
	//exported field since it begins
	//with a capital letter
	Name string
}*/

// Index : is the index handler
/*func Index(w http.ResponseWriter, r *http.Request) {
	render, err := mw.ParseDirectory("./views", "index")
	if err != nil {
		log.Fatal("Parse: ", err)
		return
	}
	render.Execute(w, map[string]string{
		"Title": "My title",
		"Body":  "This is the body",
		"tmp":   "index",
		"user":  lib.User,
	})
}*/

// Index : http://127.0.0.1:8000/
/*func Index(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
	//w.WriteHeader(http.StatusOK)
	w.Write([]byte("Gorilla!\n"))
}*/

// Index : http://127.0.0.1:8000/
// $ curl -v http://127.0.0.1:8000/
func Index(renderer *mw.Renderer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data := pongo2.Context{
			"name": "Lanti",
			"answer": func() int {
				return 42
			},
			"items": []string{
				"Apple", "Pear", "Banana", "Pineapple",
			},
		}

		if err := renderer.Render(w, "views/index.html", data); err != nil {
			log.Printf("could not execute: %s", err)
		}
	}
}
