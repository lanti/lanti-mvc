package controllers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// Articles : http://127.0.0.1:8000/articles/
func Articles(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Printing to the console!")
	fmt.Fprintln(w, "Showing all categories!")
}

// ArticlesCategory : http://127.0.0.1:8000/articles/hello-world/
func ArticlesCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fmt.Fprintf(w, "Category: %v\n", vars["category"])
}
